import logging
import datetime
from flask import Flask, abort, render_template, request, jsonify, make_response, redirect
from google.appengine.ext import ndb
import pusher, pusher.gae
import os
import helpers.utils as utils

app = Flask(__name__)

#PUSHER DETAILS

app_id = os.environ.get('PUSHER_APP_ID')
key = os.environ.get('PUSHER_APP_KEY')
secret = os.environ.get('PUSHER_APP_SECRET')

pusher_client = pusher.Pusher(
  app_id='431072',
  key='9d6daa79b39298f1201b',
  secret='766faf311c0b6f895cd8',
  cluster='us2',
  backend=pusher.gae.GAEBackend
)

# MODELS

class Event(ndb.Model):
    """Models an event."""
    activity = ndb.StringProperty(required=True)
    image = ndb.StringProperty()
    description = ndb.StringProperty()
    location = ndb.StringProperty(required=True)
    postal_code = ndb.StringProperty()
    start_at = ndb.DateTimeProperty(required=True)
    created_at = ndb.DateTimeProperty(auto_now_add=True)
    owner = ndb.KeyProperty(required=True) #maybe repeated someday if we want events to have multiple owners

class EventParticipant(ndb.Model):
    """Models the relationship of event to participants."""
    event = ndb.KeyProperty(kind='Event', required=True)
    user = ndb.KeyProperty(required=True)
    status = ndb.StringProperty(required=True) #going or interested

class Comment(ndb.Model):
    """Models an event comment."""
    event = ndb.KeyProperty(kind='Event')
    comment = ndb.StringProperty(required=True)
    posted_at = ndb.DateTimeProperty(auto_now_add=True)
    user = ndb.KeyProperty(required=True)

class User(ndb.Model):
    """Models a user."""
    handle = ndb.StringProperty(required=True)
    firstname = ndb.StringProperty()
    lastname = ndb.StringProperty()

# EVENT ROUTES

@app.route('/api/v1/events/', methods=['GET'])
def get_events():
    query = Event.query(Event.start_at > datetime.datetime.now()).order(Event.start_at)
    results = [{
        'id': event.key.id(),
        'activity': event.activity,
        'location': event.location,
        'start_at': event.start_at,
        'image': event.image,
        'description': event.description,
        'owner': utils.to_json(event.owner.get()),
        'interested': EventParticipant.query(EventParticipant.event == event.key, EventParticipant.status == 'Interested').count(),
        'going': EventParticipant.query(EventParticipant.event == event.key, EventParticipant.status == 'Going').count()
        } for event in query.fetch()]
    return jsonify({'events': results})

@app.route('/api/v1/events/postal_code/<string:postal_code>', methods=['GET'])
def get_events_by_postal_code(postal_code):
    query = Event.query(Event.postal_code == postal_code, Event.start_at > datetime.datetime.now()).order(Event.start_at)
    results = [{
        'id': event.key.id(),
        'activity': event.activity,
        'location': event.location,
        'start_at': event.start_at,
        'image': event.image,
        'description': event.description,
        'owner': utils.to_json(event.owner.get()),
        'interested': EventParticipant.query(EventParticipant.event == event.key, EventParticipant.status == 'Interested').count(),
        'going': EventParticipant.query(EventParticipant.event == event.key, EventParticipant.status == 'Going').count()
        } for event in query.fetch()]
    return jsonify({'events': results})

@app.route('/api/v1/events/<int:event_id>', methods=['GET'])
def get_event(event_id):
    ds_event = Event.get_by_id(event_id)
    if ds_event == None:
        abort(404)
    event = utils.to_json(ds_event)
    event['owner'] = utils.to_json(ds_event.owner.get())
    comments = Comment.query(Comment.event == ndb.Key('Event',event_id)).order(-Comment.posted_at)
    participants = EventParticipant.query(EventParticipant.event == ndb.Key('Event',event_id))
    event['comments'] = []
    for comment in comments.fetch():
        user = comment.user.get()
        event['comments'].append({
            'comment': comment.comment,
            'user_handle': user.handle
        })
    event['participants'] = []
    for participant in participants.fetch():
        event['participants'].append({
            'user': utils.to_json(participant.user.get()),
            'status': participant.status
        })
    return jsonify({'event': event})

@app.route('/api/v1/events', methods=['POST'])
def create_event():
    if not request.json or not 'activity' in request.json or not 'location' in request.json or not 'start_at' in request.json:
        abort(400)

    newEvent = Event(activity=request.json['activity'],
                     location=request.json['location'],
                     start_at=datetime.datetime.strptime(request.json['start_at'], "%Y-%m-%dT%H:%M"),
                     owner=ndb.Key('User', request.json['owner']))
    if 'postal_code' in request.json:
        newEvent.postal_code = request.json['postal_code']

    newEvent.put()
    return jsonify({'event': newEvent.to_dict()}), 201

@app.route('/api/v1/events/<int:event_id>', methods=['PUT'])
def update_event(event_id):
    if not request.json:
        abort(400)
    event = Event.get_by_id(event_id)
    if 'description' in request.json:
        event.description = request.json['description']
    if 'location' in request.json:
        event.location = request.json['location']
    if 'start_at' in request.json:
        event.start_at = datetime.datetime.strptime(request.json['start_at'], "%Y-%m-%dT%H:%M")
    event.put()
    retEvent = event.to_dict()
    retEvent['id'] = event_id
    return jsonify({'event': retEvent})

@app.route('/api/v1/events/<int:event_id>', methods=['DELETE'])
def delete_event(event_id):
    event = Event.get_by_id(event_id)
    if event == None:
        abort(404)
    event.key.delete()
    return jsonify({'result': True})

# COMMENT ROUTES

@app.route('/api/v1/comments', methods=['POST'])
def create_comment():
    if not request.json:
        abort(400)

    newComment = Comment(comment= request.json['comment'],
                         event= ndb.Key('Event', int(request.json['event_id'])),
                         user= ndb.Key('User', request.json['user_id']))
    newComment.put()
    pusher_message = request.json['handle'] + " : " + request.json['comment']
    pusher_client.trigger(request.json['event_id'], 'new-comment', {'message': pusher_message})
    return jsonify({'comment': utils.to_json(newComment)}), 201

# PARTICIPANT ROUTES

@app.route('/api/v1/event_participants', methods=['POST'])
def create_event_participant():
    if not request.json:
        abort(400)
    eps = EventParticipant.query(EventParticipant.event == ndb.Key('Event',int(request.json['event_id'])), EventParticipant.user == ndb.Key('User', request.json['user_id']))
    existing_ep = eps.get()
    status = request.json['status']
    if (status != 'Going' and status != 'Interested'):
        if (existing_ep):
            existing_ep.key.delete()
        return jsonify({'status': 'Not Going'}), 201
    if (existing_ep):
        newEP = existing_ep
        newEP.status = request.json['status']
    else:
        newEP = EventParticipant(status= request.json['status'],
                                 event= ndb.Key('Event', int(request.json['event_id'])),
                                 user= ndb.Key('User', request.json['user_id']))
    newEP.put()
    return jsonify({'event_participant': utils.to_json(newEP)}), 201

# USER ROUTES
@app.route('/api/v1/users/<string:user_id>', methods=['GET'])
def get_user(user_id):
    user = User.get_by_id(user_id)
    if user is None:
        abort(404)
    return jsonify({'user': utils.to_json(user)})

@app.route('/api/v1/users', methods=['POST'])
def create_user():
    if not request.json:
        abort(400)
    user = User.get_by_id(request.json['user_id'])
    if user is None:
        newUser = User(id=request.json['user_id'],
                       handle=request.json['handle'])
        newUser.put()
        return jsonify({'user': newUser.to_dict()}), 201
    else:
        return jsonify({'user': request.json}), 201

@app.route('/api/v1/users', methods=['PUT'])
def update_user():
    if not request.json:
        abort(400)
    user = User.get_by_id(request.json['user_id'])
    if user is None:
        abort(400)
    if 'handle' in request.json:
        user.handle = request.json['handle']
    if 'firstname' in request.json:
        user.firstname = request.json['firstname']
    if 'lastname' in request.json:
        user.lastname = request.json['lastname']
    user.put()
    return jsonify({'user': user.to_dict()}), 201

@app.route('/api/v1/user_events/<int:user_id>', methods=['GET'])
def get_user_events(user_id):
    query = Event.query(Event.start_at > datetime.datetime.now(), Event.owner == ndb.Key('User',str(user_id))).order(Event.start_at)
    owner_of = [{
        'id': event.key.id(),
        'activity': event.activity,
        'location': event.location,
        'start_at': event.start_at,
        'image': event.image,
        'description': event.description,
        'interested': EventParticipant.query(EventParticipant.event == event.key, EventParticipant.status == 'Interested').count(),
        'going': EventParticipant.query(EventParticipant.event == event.key, EventParticipant.status == 'Going').count()
        } for event in query.fetch()]
    interested_query = EventParticipant.query(EventParticipant.user == ndb.Key('User',str(user_id)), EventParticipant.status == 'Interested')
    interested_in = []
    for ep in interested_query.fetch():
        event = ep.event.get()
        interested_in.append({
            'id': event.key.id(),
            'activity': event.activity,
            'location': event.location,
            'start_at': event.start_at,
            'image': event.image,
            'description': event.description,
            'interested': EventParticipant.query(EventParticipant.event == event.key, EventParticipant.status == 'Interested').count(),
            'going': EventParticipant.query(EventParticipant.event == event.key, EventParticipant.status == 'Going').count()
        })
    going_query = EventParticipant.query(EventParticipant.user == ndb.Key('User',str(user_id)), EventParticipant.status == 'Going')
    going_to = []
    for ep in going_query.fetch():
        event = ep.event.get()
        going_to.append({
            'id': event.key.id(),
            'activity': event.activity,
            'location': event.location,
            'start_at': event.start_at,
            'image': event.image,
            'description': event.description,
            'interested': EventParticipant.query(EventParticipant.event == event.key, EventParticipant.status == 'Interested').count(),
            'going': EventParticipant.query(EventParticipant.event == event.key, EventParticipant.status == 'Going').count()
        })
    return jsonify({'owner_of': owner_of, 'interested_in': interested_in, 'going_to': going_to})

# ERROR ROUTES

@app.errorhandler(404)
def not_found(error):
    logging.exception('An error occurred during a request.')
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return make_response(jsonify({'error': 'An internal error occurred.'}), 500)
