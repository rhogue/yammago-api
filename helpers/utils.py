import datetime
from google.appengine.ext import ndb

#source: https://stackoverflow.com/questions/25871308/app-engine-datastore-to-dict-alternative-to-serialize-ndb-model-to-json
def to_json(o):
    if isinstance(o, list):
        return [to_json(l) for l in o]
    if isinstance(o, dict):
        x = {}
        for l in o:
            x[l] = to_json(o[l])
        return x
    if isinstance(o, datetime.datetime):
        return o.isoformat()
    if isinstance(o, ndb.GeoPt):
        return {'lat': o.lat, 'lon': o.lon}
    if isinstance(o, ndb.Key):
        return o.id()
    if isinstance(o, ndb.Model):
        dct = o.to_dict()
        dct['id'] = o.key.id()
        return to_json(dct)
    return o
