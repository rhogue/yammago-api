# yammago project

Steps to run locally:

1. Clone the project
1. If you don't already have it, download the SDK for Google App Engine: https://cloud.google.com/appengine/docs/standard/python/download
1. From the command line, run `pip install -t lib -r requirements.txt`
1. OPTIONAL: start a local environment (Seth found this solved a few of his local deployment issues)
    1. If you don't have virtualenv, install it using `sudo pip install virtualenv`
    1. `virtualenv env`
    1. `source env/bin/activate`
    1. When you are done running the application, just run `deactivate`
1. To start the application, run `dev_appserver.py app.yaml` from the root directory of the project. The localhost port will be shown, but it is typically http://localhost:8080/